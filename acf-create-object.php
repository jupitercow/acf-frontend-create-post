<?php
/*
Plugin Name: Advanced Custom Fields: Create Object
Plugin URI: http://bitbucket.org/jupitercow/acf-create-post
Description: Very basic plugin that allows a basic interface for creating posts or users from the front end through ACF.
Version: 1.0.0
Author: jcow
Author URI: http://jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class acf_create_object
{
	/**
	 * The default status for new posts
	 */
	public static $prefix = __CLASS__;

	/**
	 * The default settings
	 */
	public static $settings = array(
		'post_status' => 'draft',
		'post_title'  => "New Post",
		'post_type'   => 'post'
	);

	/**
	 * Initialize the Class
	 *
	 * Add filters and actions and set up the options.
	 *
	 * @author  Jake Snyder
	 * @date	24/08/13
	 */
	public function init()
	{
		// Create the new object
		add_filter( 'acf/pre_save_post',             array(__CLASS__, 'create_object'), 1 );
	}

	/**
	 * Allow Post/User Creation
	 *
	 * This takes the "post_id" argument passed to the "acf_form()" form, and if, instead of a number, it is "new_post", 
	 * it will create a new post and return that id for editing.
	 *
	 * @author  Jake Snyder
	 * @date	24/08/13
	 *
	 * @type	filter
	 *
	 * @param	int|string	$post_id id of the post you want to edit, or a string to instruct creating a new object
	 * @return	void
	 */
	public static function create_object( $post_id )
	{
		$create_types = array( 'new_post' );

		// check if this is to be a new object
		if (! in_array($post_id, $create_types) ) return $post_id;

		/**
		 * Create a new post
		 */
		$post = array(
			'post_status' => (! empty($_POST['post_status']) ? $_POST['post_status'] : apply_filters( self::$prefix . '/post/status', self::$settings['post_status'] )),
			'post_title'  => (! empty($_POST['post_title']) ? $_POST['post_title'] : apply_filters( self::$prefix . '/post/title', self::$settings['post_title'] )),
			'post_type'   => (! empty($_POST['post_type']) ? $_POST['post_type'] : apply_filters( self::$prefix . '/post/type', self::$settings['post_type'] ))
		);

		// insert the post
		$post_id = wp_insert_post( $post );
		do_action( self::$prefix . '/post/created', $post_id );

		return $post_id;
	}
}

acf_create_object::init();